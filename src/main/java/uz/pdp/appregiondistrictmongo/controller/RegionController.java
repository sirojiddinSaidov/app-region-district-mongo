package uz.pdp.appregiondistrictmongo.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appregiondistrictmongo.collection.District;
import uz.pdp.appregiondistrictmongo.collection.Region;
import uz.pdp.appregiondistrictmongo.payload.DistrictDTO;
import uz.pdp.appregiondistrictmongo.payload.RegionDTO;
import uz.pdp.appregiondistrictmongo.repository.DistrictRepository;
import uz.pdp.appregiondistrictmongo.repository.RegionRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/region")
@RequiredArgsConstructor
public class RegionController {

    private final RegionRepository regionRepository;
    private final DistrictRepository districtRepository;

    @PostMapping
    public HttpEntity<?> add(@RequestBody RegionDTO regionDTO) {
        Region region = regionRepository.save(Region.builder()
                .name(regionDTO.getName())
                .build());
        return ResponseEntity.status(201).body(region);
    }

    @GetMapping
    public HttpEntity<?> list() {

        List<Region> regions = regionRepository.findAll();

        List<RegionDTO> regionDTOList = regions.stream().map(region -> RegionDTO.builder()
                        .name(region.getName())
                        .id(region.getId())
                        .districts(districtDTOList(region))
                        .build())
                .collect(Collectors.toList());

        return ResponseEntity.ok(regionDTOList);
    }

    private List<DistrictDTO> districtDTOList(Region region) {

        List<District> districts = districtRepository.findAllByRegionId(region.getId());

        List<DistrictDTO> districtDTOList = new LinkedList<>();

        districts.forEach(district -> districtDTOList.add(DistrictDTO.builder()
                .name(district.getName())
                .regionId(district.getRegionId())
                .id(district.getId())
                .build()));

        return districtDTOList;
    }
}
