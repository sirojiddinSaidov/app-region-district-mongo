package uz.pdp.appregiondistrictmongo.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appregiondistrictmongo.collection.District;
import uz.pdp.appregiondistrictmongo.collection.Region;
import uz.pdp.appregiondistrictmongo.payload.DistrictDTO;
import uz.pdp.appregiondistrictmongo.payload.RegionDTO;
import uz.pdp.appregiondistrictmongo.repository.DistrictRepository;
import uz.pdp.appregiondistrictmongo.repository.RegionRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/district")
@RequiredArgsConstructor
public class DistrictController {

    private final DistrictRepository districtRepository;

    @PostMapping
    public HttpEntity<?> add(@RequestBody DistrictDTO districtDTO) {
        District district = districtRepository.save(
                District.builder()
                        .name(districtDTO.getName())
                        .regionId(districtDTO.getRegionId())
                        .build());
        return ResponseEntity.status(201).body(district);
    }

    @GetMapping
    public HttpEntity<?> list() {
        return null;
    }

    private List<DistrictDTO> districtDTOList(Region region) {
        return new LinkedList<>();
    }
}
