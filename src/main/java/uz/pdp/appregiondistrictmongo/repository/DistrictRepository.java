package uz.pdp.appregiondistrictmongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import uz.pdp.appregiondistrictmongo.collection.District;

import java.util.List;

public interface DistrictRepository extends MongoRepository<District, String> {

    List<District> findAllByRegionId(String regionId);
}
