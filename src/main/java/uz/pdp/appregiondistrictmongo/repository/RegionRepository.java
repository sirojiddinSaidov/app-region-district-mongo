package uz.pdp.appregiondistrictmongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import uz.pdp.appregiondistrictmongo.collection.Region;


public interface RegionRepository extends MongoRepository<Region,String> {
}
