package uz.pdp.appregiondistrictmongo.payload;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegionDTO {

    private String id;

    @NotBlank
    private String name;

    private List<DistrictDTO> districts;
}
