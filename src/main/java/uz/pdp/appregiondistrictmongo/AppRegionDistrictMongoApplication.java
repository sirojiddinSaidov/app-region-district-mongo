package uz.pdp.appregiondistrictmongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppRegionDistrictMongoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppRegionDistrictMongoApplication.class, args);
    }

}
